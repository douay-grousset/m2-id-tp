package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Reservation.class)
public abstract class Reservation_ {

	public static volatile SingularAttribute<Reservation, IdReservation> idReservation;
	public static volatile SingularAttribute<Reservation, DefClasse> defClasse;
	public static volatile SingularAttribute<Reservation, Integer> nbPlaces;
	public static volatile SingularAttribute<Reservation, Client> client;

}

