package entities;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name="reservations", schema="TP1")
public class Reservation {
    @EmbeddedId
    IdReservation idReservation;
    
    @ManyToOne
    @JoinColumn(name="numCl")
    @MapsId("numCl")
    private Client client;
    
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name="numVol", referencedColumnName="numVol"),
        @JoinColumn(name="classe", referencedColumnName="classe")
    })
    @MapsId("idDefClasse")
    private DefClasse defClasse;
    
    @Column
    private int nbPlaces;
}
