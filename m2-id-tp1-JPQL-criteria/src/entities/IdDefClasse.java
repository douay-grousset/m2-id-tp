package entities;

import java.io.Serializable;

@SuppressWarnings("serial")
public class IdDefClasse implements Serializable{
    private String vol;
    private String classe;
   
    public IdDefClasse() {}
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((classe == null) ? 0 : classe.hashCode());
        result = prime * result + ((vol == null) ? 0 : vol.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        IdDefClasse other = (IdDefClasse) obj;
        if (classe == null) {
            if (other.classe != null)
                return false;
        } else if (!classe.equals(other.classe))
            return false;
        if (vol == null) {
            if (other.vol != null)
                return false;
        } else if (!vol.equals(other.vol))
            return false;
        return true;
    }
}
