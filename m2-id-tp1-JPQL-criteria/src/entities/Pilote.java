package entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="pilotes", schema="TP1")
public class Pilote {
    @Id
    @Column
    private Integer numPil;
    @Column
    private String nomPil;
    @Column
    private Integer naisPil;
    @Column
    private String villePil;
}
