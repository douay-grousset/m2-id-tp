package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Avion.class)
public abstract class Avion_ {

	public static volatile SingularAttribute<Avion, String> nomAv;
	public static volatile SingularAttribute<Avion, Integer> capAv;
	public static volatile SingularAttribute<Avion, String> villeAv;
	public static volatile SingularAttribute<Avion, Integer> numAv;

}

