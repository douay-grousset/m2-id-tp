package entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="avions", schema="TP1")
public class Avion {
    @Id
    @Column
    private int numAv;
    @Column
    private String nomAv;
    @Column
    private int capAv;
    @Column
    private String villeAv;
}
