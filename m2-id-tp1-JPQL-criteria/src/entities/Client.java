package entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="clients", schema="TP1")
public class Client {
    @Id
    @Column
    private Integer numCl;
    @Column
    private String nomCl;
    @Column
    private Integer numRueCl;
    @Column
    private String nomRueCl;
    @Column
    private Integer codePosteCl;
    @Column
    private String villeCl;
}
