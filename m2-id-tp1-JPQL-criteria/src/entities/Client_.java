package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Client.class)
public abstract class Client_ {

	public static volatile SingularAttribute<Client, String> nomRueCl;
	public static volatile SingularAttribute<Client, Integer> numRueCl;
	public static volatile SingularAttribute<Client, Integer> codePosteCl;
	public static volatile SingularAttribute<Client, String> nomCl;
	public static volatile SingularAttribute<Client, Integer> numCl;
	public static volatile SingularAttribute<Client, String> villeCl;

}

