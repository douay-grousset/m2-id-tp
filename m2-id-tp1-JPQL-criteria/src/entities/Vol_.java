package entities;

import java.sql.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Vol.class)
public abstract class Vol_ {

	public static volatile SingularAttribute<Vol, Integer> coutVol;
	public static volatile SingularAttribute<Vol, String> villeD;
	public static volatile SingularAttribute<Vol, Pilote> pilote;
	public static volatile SingularAttribute<Vol, String> numVol;
	public static volatile SingularAttribute<Vol, String> villeA;
	public static volatile SingularAttribute<Vol, Date> dateD;
	public static volatile SingularAttribute<Vol, Date> dateA;
	public static volatile SingularAttribute<Vol, Avion> avion;

}

