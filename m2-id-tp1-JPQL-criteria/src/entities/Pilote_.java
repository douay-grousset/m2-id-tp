package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Pilote.class)
public abstract class Pilote_ {

	public static volatile SingularAttribute<Pilote, String> nomPil;
	public static volatile SingularAttribute<Pilote, String> villePil;
	public static volatile SingularAttribute<Pilote, Integer> numPil;
	public static volatile SingularAttribute<Pilote, Integer> naisPil;

}

