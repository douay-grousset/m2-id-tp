package entities;

import java.io.Serializable;

import javax.persistence.Embeddable;

@SuppressWarnings("serial")
@Embeddable
public class IdReservation implements Serializable {
    
    private int numCl;
    private IdDefClasse idDefClasse;

    public IdReservation() {}

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((idDefClasse == null) ? 0 : idDefClasse.hashCode());
        result = prime * result + numCl;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        IdReservation other = (IdReservation) obj;
        if (idDefClasse == null) {
            if (other.idDefClasse != null)
                return false;
        } else if (!idDefClasse.equals(other.idDefClasse))
            return false;
        if (numCl != other.numCl)
            return false;
        return true;
    }

}
