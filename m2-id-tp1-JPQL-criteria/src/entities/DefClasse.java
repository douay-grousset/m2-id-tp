package entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="defclasses", schema="TP1")
@IdClass(IdDefClasse.class)
public class DefClasse {
    @Id
    @ManyToOne
    @JoinColumn(name="numVol")
    private Vol vol;
    @Id
    @Column
    private String classe;
    @Column
    private BigDecimal coeffPlace;
    @Column
    private BigDecimal coeffPrix;
}
