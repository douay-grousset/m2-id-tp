package entities;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DefClasse.class)
public abstract class DefClasse_ {

	public static volatile SingularAttribute<DefClasse, Vol> vol;
	public static volatile SingularAttribute<DefClasse, String> classe;
	public static volatile SingularAttribute<DefClasse, BigDecimal> coeffPlace;
	public static volatile SingularAttribute<DefClasse, BigDecimal> coeffPrix;

}

