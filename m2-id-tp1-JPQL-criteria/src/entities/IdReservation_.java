package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IdReservation.class)
public abstract class IdReservation_ {

	public static volatile SingularAttribute<IdReservation, IdDefClasse> idDefClasse;
	public static volatile SingularAttribute<IdReservation, Integer> numCl;

}

