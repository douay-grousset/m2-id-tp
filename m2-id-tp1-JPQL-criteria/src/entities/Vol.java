package entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="vols", schema="TP1")
public class Vol {
    @Id
    @Column
    private String numVol;
    @Column
    private String villeD;
    @Column
    private String villeA;
    @Column
    private Date dateD;
    @Column
    private Date dateA;
    @ManyToOne
    @JoinColumn(name="numPil")
    private Pilote pilote;
    @ManyToOne
    @JoinColumn(name="numAv")
    private Avion avion;
    @Column
    private int coutVol;
    
}
