package org.example;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import entities.Client;
import entities.Pilote;

public class TestJpql {
    
    private static EntityManagerFactory emf;
    private static EntityManager em;
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        emf = Persistence.createEntityManagerFactory("jpa");
        em = emf.createEntityManager();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        em.close();
        emf.close();
    }

    @Test
    public void testA() {
//      a) nombre de vols assurés par le pilote 5050
        final String QUERY_A = "SELECT COUNT(*) FROM Vol WHERE NumPil=5050";
        
        Long result = em.createQuery(QUERY_A, Long.class).getSingleResult();
        assertEquals(62L, result.longValue());
    }
    
    @Test
    public void testB() {
//      b) nb places réservées sur le vol v101
        final String QUERY_B = "SELECT SUM(nbPlaces) FROM Reservation WHERE NumVol='V101'";
        
        Long result = em.createQuery(QUERY_B, Long.class).getSingleResult();
        assertEquals(50L, result.longValue());
    }
    
    @Test
    public void testC() {
//      c) Get the clients who reserved on a flight on which Mr Lorentz also reserved
//        SQL QUERY
//        SELECT DISTINCT CLIENTS.NUMCL, CLIENTS.NOMCL
//        FROM TP1.CLIENTS clients
//        JOIN TP1.RESERVATIONS RESA ON CLIENTS.NUMCL=RESA.NUMCL
//        WHERE RESA.NUMVOL IN (
//              SELECT RESA.NUMVOL
//              FROM TP1.CLIENTS CLIENTS, TP1.RESERVATIONS RESA
//              WHERE CLIENTS.NUMCL=RESA.NUMCL
//              AND CLIENTS.NOMCL='Lorentz')
//          AND CLIENTS.NOMCL != 'Lorentz';
        final String QUERY_C = 
                "SELECT DISTINCT client " // or resa.client
               +"FROM Reservation resa "
               +"WHERE resa.defClasse.vol IN ( "
                   +"SELECT resa2.defClasse.vol "
                   +"FROM Reservation resa2 join resa2.client c "
                   +"WHERE c.nomCl='Lorentz' "
                   +") "
               +"AND resa.client.nomCl != 'Lorentz'";
        List<Client> results = em.createQuery(QUERY_C, Client.class).getResultList();
        assertEquals(19, results.size());
        // TODO make more assertions
    }
   
    @Test
    public void testD() {
//      d) Get all the pilots who have not flown from Paris (VilleD)
//      SELECT *
//      FROM TP1.PILOTES PIL
//      WHERE PIL.NUMPIL NOT IN
//            (SELECT NUMPIL
//             FROM TP1.VOLS VOLS
//             WHERE VOLS.VILLED = 'Paris');
      String QUERY_D = 
       "SELECT p "
      +"FROM Pilote p "
      +"WHERE p.numPil NOT IN "
            +"(SELECT v.pilote.numPil " 
             +"FROM Vol v "
             +"WHERE villeD = 'Paris')";
        List<Pilote> results = em.createQuery(QUERY_D, Pilote.class).getResultList();
        assertEquals(19, results.size());
        // TODO make more assertions
    }

    @Test
    public void testE() {
//      e) For each pilot, get the number of different planes that has been piloted by the pilot
//        SELECT NUMPIL, COUNT(DISTINCT NUMAV)
//        FROM TP1.VOLS VOLS
//        GROUP BY VOLS.NUMPIL;
        final String QUERY =
         "SELECT v.pilote.numPil, COUNT(DISTINCT v.avion.numAv) "
        +"FROM Vol v "
        +"GROUP BY v.pilote.numPil";

        @SuppressWarnings("unchecked")
        List<Object[]> results = em.createQuery(QUERY).getResultList();

        assertEquals(16, results.size());
        for (Object[] result : results) {
            System.out.print("numPil: " + (Integer)result[0]);
            System.out.println(" count: " + (Long)result[1]);
        }
        // TODO make more assertions
    }

    @Test
    public void testF() {
//      f) Number of seats reserved for each class on the flight V601
        String QUERY =
                "SELECT Resa.defClasse.classe, SUM(Resa.nbPlaces) "
               +"FROM Reservation Resa "
               +"WHERE Resa.defClasse.vol.numVol = 'V601' "
               +"GROUP BY Resa.defClasse.classe";

        @SuppressWarnings("unchecked")
        List<Object[]> results = em.createQuery(QUERY).getResultList();

        assertEquals(3, results.size());
        Object[] firstTuple = results.get(0);
        Object[] secondTuple = results.get(1);
        Object[] thirdTuple = results.get(2);
        
        assertEquals("Business", firstTuple[0]);
        assertEquals(7L, firstTuple[1]);
        assertEquals("Economique", secondTuple[0]);
        assertEquals(14L, secondTuple[1]);
        assertEquals("Touriste", thirdTuple[0]);
        assertEquals(1L, thirdTuple[1]);
    }

}
