package org.example;

import static org.junit.Assert.assertEquals;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import entities.Client;
import entities.Pilote_;
import entities.Vol;
import entities.Vol_;

public class TestCriteriaQuery {
    
    private static EntityManagerFactory emf;
    private static EntityManager em;
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        emf = Persistence.createEntityManagerFactory("jpa");
        em = emf.createEntityManager();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        em.close();
        emf.close();
    }

    @Test
    public void testA() {
//      a) nombre de vols assurés par le pilote 5050
//      SELECT COUNT(*)
//      FROM TP1.VOLS
//      WHERE NumPil=5050;
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Vol> vol = cq.from(Vol.class);
        Expression<Long> exp = cb.count(vol);
        // If one wants to count something else than *
//        Expression<Long> exp = cb.count(vol.get(Vol_.pilote).get(Pilote_.numPil));
        
        Path<Integer> path = vol.get(Vol_.pilote).get(Pilote_.numPil);
        Predicate pred = cb.equal(path, 5050);
        
        cq.where(pred).select(exp);
        
        TypedQuery<Long> query = em.createQuery(cq);
        Long result = query.getSingleResult();
        
        assertEquals(62L, result.longValue());
    }

    @Test
    public void testABis() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Vol> vol = cq.from(Vol.class);

        cq
        .select(cb.count(vol))
        .where(cb.equal(vol.get(Vol_.pilote), 5050));

        // this where also works
//        .where(cb.equal(vol.get(Vol_.pilote).get(Pilote_.numPil), 5050));
        
        TypedQuery<Long> query = em.createQuery(cq);
        Long result = query.getSingleResult();
        
        assertEquals(62L, result.longValue());
    }

    @Test
    @Ignore
    public void testB() {
//      b) nb places réservées sur le vol v101
    }
    
    @Test
    @Ignore
    public void testC() {
//      c) Get the clients who reserved on a flight on which Mr Lorentz also reserved
//        SQL QUERY
//        SELECT DISTINCT CLIENTS.NUMCL, CLIENTS.NOMCL
//        FROM TP1.CLIENTS clients
//        JOIN TP1.RESERVATIONS RESA ON CLIENTS.NUMCL=RESA.NUMCL
//        WHERE RESA.NUMVOL IN (
//              SELECT RESA.NUMVOL
//              FROM TP1.CLIENTS CLIENTS, TP1.RESERVATIONS RESA
//              WHERE CLIENTS.NUMCL=RESA.NUMCL
//              AND CLIENTS.NOMCL='Lorentz')
//          AND CLIENTS.NOMCL != 'Lorentz';
    }

    @Test
    @Ignore
    public void testD() {
//      d) Get all the pilots who have not flown from Paris (VilleD)
//      SELECT *
//      FROM TP1.PILOTES PIL
//      WHERE PIL.NUMPIL NOT IN
//            (SELECT NUMPIL
//             FROM TP1.VOLS VOLS
//             WHERE VOLS.VILLED = 'Paris');
    }

    @Test
    @Ignore
    public void testE() {
//      e) For each pilot, get the number of different planes that has been piloted by the pilot
//        SELECT NUMPIL, COUNT(DISTINCT NUMAV)
//        FROM TP1.VOLS VOLS
//        GROUP BY VOLS.NUMPIL;
    }

    @Test
    @Ignore
    public void testF() {
//      f) Number of seats reserved for each class on the flight V601
    }

}
