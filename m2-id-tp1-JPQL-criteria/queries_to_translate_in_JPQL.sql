-- a) nombre de vols assurés par le pilote 5050
SELECT COUNT(*)
FROM TP1.VOLS
WHERE NumPil=5050;

-- b) nb places réservées sur le vol v101
SELECT SUM(NBPLACES) FROM TP1.RESERVATIONS WHERE NumVol='V101';

-- c) Get the clients who reserved on a flight on which Mr Lorentz also reserved
SELECT DISTINCT CLIENTS.NUMCL, CLIENTS.NOMCL
FROM TP1.CLIENTS clients
JOIN TP1.RESERVATIONS RESA ON CLIENTS.NUMCL=RESA.NUMCL
WHERE RESA.NUMVOL IN
      (SELECT RESA.NUMVOL
       FROM TP1.CLIENTS CLIENTS, TP1.RESERVATIONS RESA
       WHERE CLIENTS.NUMCL=RESA.NUMCL
       AND CLIENTS.NOMCL='Lorentz')
  AND CLIENTS.NOMCL != 'Lorentz';

-- d) Get all the pilots who have not flight from Paris (VilleD)
SELECT *
FROM TP1.PILOTES PIL
WHERE PIL.NUMPIL NOT IN
      (SELECT NUMPIL
       FROM TP1.VOLS VOLS
       WHERE VOLS.VILLED = 'Paris');

-- e) For each pilot, get the number of different planes that has been piloted by the pilot
SELECT NUMPIL--, COUNT(DISTINCT NUMAV)
FROM TP1.VOLS VOLS
WHERE NUMPIL IS NOT NULL;
--GROUP BY VOLS.NUMPIL;

-- bonus: For each pilot, the number of times he has flown each individual plane
SELECT NUMPIL, NUMAV, COUNT(*)
FROM TP1.VOLS VOLS
GROUP BY VOLS.NUMPIL, NUMAV;

-- f) Number of seats reserved for each class on the flight V601
SELECT Classe, SUM(NBPLACES)
FROM TP1.RESERVATIONS RESA
WHERE RESA.NUMVOL = 'V601'
GROUP BY RESA.CLASSE;