package org.example;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.Adresse;
import entities.Civilite;
import entities.Personne;
import entities.TypeAdresse;

public class Test {
    private static EntityManagerFactory emf;
    private static EntityManager em;
    
    
    public static void main(String[] args) {
        debutTest();
        test1();
        finTest();
    }

    private static void debutTest() {
        emf = Persistence.createEntityManagerFactory("jpa");
        em = emf.createEntityManager();
    }

    private static void finTest() {
        em.close();
        emf.close();
    }
    
    private static void test1() {
        
        em.getTransaction().begin();
        
        Personne p = new Personne();
        p.getAdresses().put(TypeAdresse.PRINCIPALE, new Adresse("18 rue jean de boufigue","13128","Vitroule"));
        p.getAdresses().put(TypeAdresse.SECONDAIRE, new Adresse("19 avenue des nope nope nope","75000","Hell"));
        p.getPrenoms().add("Romaric");
        p.getPrenoms().add("Stephano");
        p.getPrenoms().add("Cunégonde");
        em.persist(p);
        em.getTransaction().commit();
        
        int idPersonne = p.getId(); 
        
        test2(idPersonne);
        test3(idPersonne);
        testDelete(idPersonne);
    }

    private static void test2(int idPersonne) {
        em.getTransaction().begin();

        Personne p = em.find(Personne.class,idPersonne);
        p.getPrenoms().remove(0);
        p.setCivilite(Civilite.DIVORCE);
        p.getAdresses().get(TypeAdresse.PRINCIPALE).setVille("trou ville");

        em.getTransaction().commit();
    }
    
    private static void test3(int idPersonne) {
        Personne p = em.find(Personne.class,idPersonne);
        em.close();

        em = emf.createEntityManager();
        em.getTransaction().begin();

        p.setNom("Duloup");
        em.merge(p);

        em.getTransaction().commit();
    }

    private static void testDelete(int idPersonne) {
        em.getTransaction().begin();
        Personne p = em.find(Personne.class,idPersonne);
        em.remove(p);
        em.getTransaction().commit();
    }
}
