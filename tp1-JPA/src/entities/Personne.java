package entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKeyEnumerated;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="personne")
@Table(name="personnes")
public class Personne {
    private Integer id;
    private String nom;
    private List<String> prenoms;
    private Map<TypeAdresse,Adresse> adresses;
    private Date dateNaissance;
    private Civilite civilite;

    public Personne() {
        this.prenoms = new ArrayList<>();
        this.adresses = new HashMap<TypeAdresse,Adresse>();
    }

    @Id
    @Column(name="ID_PER")
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Integer getId() {return id;}
    public void setId(Integer id) {this.id = id;}

    @Column(name="NOM_PER")
    public String getNom() {return nom;}
    public void setNom(String nom) {this.nom = nom;}

    @ElementCollection
    @Column(name="PRENOM_PER")
    public List<String> getPrenoms() {return prenoms;}
    public void setPrenoms(List<String> prenoms) {this.prenoms = prenoms;}

    @ElementCollection
    @MapKeyEnumerated(EnumType.STRING)
    public Map<TypeAdresse, Adresse> getAdresses() {return adresses;}
    public void setAdresses(Map<TypeAdresse, Adresse> adresses) {this.adresses = adresses;}

    @Column(name="DATE_NAISSANCE_PER")
    @Temporal(TemporalType.DATE)
    public Date getDateNaissance() {return dateNaissance;}
    public void setDateNaissance(Date dateNaissance) {this.dateNaissance = dateNaissance;}

    @Enumerated(EnumType.STRING)
    public Civilite getCivilite() {return civilite;}
    public void setCivilite(Civilite civilite) {this.civilite = civilite;}
}
