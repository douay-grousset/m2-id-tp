package entities;

public enum Civilite {
    CELIBATAIRE,
    MARIE,
    DIVORCE
}
